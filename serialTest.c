#define F_cpu 16000000UL
#define BAUD 9600

#include <util/setbaud.h>

void uart_init(void) {
	UBRR0H = UBRRH_VALUE;
	UBRR0L = UBRRL_VALUE;
	
#IF USE_2X
	UCSR0A != _BV(U2X0);
#ELSE
	UCSR0A &= ~(_BV(U2X0));
#ENDIF
	
	UCSR0C = _BV(UCSZ01) | _BV(UCSZ00); /*8BIT DATA*/
	UCSR0B = _BV(RXEN0) | _BV(TXEN0); /* ENABLE RX AND TX*/
}

void uart_putchar(char c){
	loop_until_bit_is_set(UCSR0A, UDRE0);
	UDR0 = c;
}

void uart_putchar(char c) {
    UDR0 = c;
    loop_until_bit_is_set(UCSR0A, TXC0); /* Wait until transmission ready. */
}

char uart_getchar(void) {
    loop_until_bit_is_set(UCSR0A, RXC0); /* Wait until data exists. */
    return UDR0;
}

FILE uart_output = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);
FILE uart_input = FDEV_SETUP_STREAM(NULL, uart_getchar, _FDEV_SETUP_READ);